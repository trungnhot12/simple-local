# CHANGELOG

## 13/01/2020

- Add JPEG support for ext gd of phpfpm 7.2 image
  - Ref: https://github.com/docker-library/php/issues/931#issuecomment-568658449 
    ```
      RUN docker-php-ext-configure gd
      --with-png-dir=/usr/include/
      --with-jpeg-dir=/usr/include/
      --with-freetype-dir=/usr/include/
    ```
  - Ref: https://stackoverflow.com/questions/48169044/docker-alpine-enable-gd-jpeg-support 

## 12/01/2020

- Fix `iconv` function for phpfpm 7.2 image 
  - Issue: https://github.com/nunomaduro/phpinsights/issues/43#issuecomment-498108857 